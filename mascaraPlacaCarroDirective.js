appRastrus.directive("mascaraPlaca", function(){
	return {
		require: "ngModel",
		link: function(scope, element, attrs, ctrl){
			var _fromatPlacaCarro = function(placa){
				placa = placa.toUpperCase();

				if (placa.length >= 1 && placa.length <= 3){					
					placa = placa.replace(/[^A-Z]+/g, '');
				}
				
				if (placa.length == 4)	
					placa = placa.substring(0,3)+"-"+placa.substring(3);
				
				if (placa.length >= 5 && placa.length <= 8){
					placa = placa.substring(0,4)+placa.replace(/[^0-9]+/g, '');
				}

				return placa;			
			}
			 element.bind("keyup", function(){
			 	ctrl.$setViewValue(_fromatPlacaCarro(ctrl.$viewValue));
			 	ctrl.$render();				
			 });				
		}
	};
});
