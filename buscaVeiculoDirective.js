appRastrus.directive('buscaveiculo',function(){
	return {
	    templateUrl:'view/busca-veiculo.html',
	    controller:'buscaVeiculoController',
	    restrict: 'E',
	    replace: true,
	}
});