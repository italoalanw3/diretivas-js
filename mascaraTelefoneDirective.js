appRastrus.directive("mascaraTelefone", function(){
	return {
		require: "ngModel",
		link: function(scope, element, attrs, ctrl){
			var _fromatTelefone = function(telefone){
				if (telefone != null) {
					telefone = telefone.replace(/[^0-9]+/g, '');

					if (telefone.length < 11){
						if(telefone.length > 2) {
							telefone = "("+telefone.substring(0,2)+") " + telefone.substring(2);
		                }
						if(telefone.length > 9) {
							telefone = telefone.substring(0,9)+"-" + telefone.substring(9,13);
		                }
	            	} else
	            	{
						if(telefone.length > 2) {
							telefone = "("+telefone.substring(0,2)+") " + telefone.substring(2);
		                }
						if(telefone.length > 10) {
							telefone = telefone.substring(0,10)+"-" + telefone.substring(10,14);
		                }            		
	            	}
	            }

	            return telefone;
			}
			element.bind("keyup", function(){
				ctrl.$setViewValue(_fromatTelefone(ctrl.$viewValue));
				ctrl.$render();				
			});				
		}
	};
});