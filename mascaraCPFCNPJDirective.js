appRastrus.directive("mascaraCpfcnpj", function($filter){
	return {
		require: "ngModel",
		link: function(scope, element, attrs, ctrl){
			var _frormatCpfCnpj = function(cpfCnpj){				
				if (cpfCnpj != null) {
					cpfCnpj = cpfCnpj.replace(/[^0-9]+/g, '');
					var cpfCnpj_F = cpfCnpj;				
					if (cpfCnpj != null) {					
						if (cpfCnpj.length == 11)
						{
							cpfCnpj_F = cpfCnpj.substr(0, 3) + '.' + cpfCnpj.substr(3, 3) + '.' + cpfCnpj.substr(6, 3) + '-' + cpfCnpj.substr(9,2);
						}	
						else if(cpfCnpj.length >= 14)
						{
							cpfCnpj_F = cpfCnpj.substr(0,2) + '.' + cpfCnpj.substr(2,3) + '.' +cpfCnpj.substr(5,3) + '/' + cpfCnpj.substr(8,4) + '-' + cpfCnpj.substr(12,2);
						}
		            }	            
		        }
	            return cpfCnpj_F;
			}

			element.bind("keyup", function(){
				ctrl.$setViewValue(_frormatCpfCnpj(ctrl.$viewValue));							
				ctrl.$render();				
			});	

			ctrl.$parsers.push(function (value){
				if (value.length === 14 || value.length === 18)
					value = value.replace(/[^0-9]+/g, '');				
				return value;
			});
			
			ctrl.$formatters.push(function(value){			
				if (value != null)
				{
					if (value.length === 11)
						return $filter("mascaraCPF")(value);
					if (value.length === 14)
						return $filter("mascaraCNPJ")(value);				
				}
				return value;				
			});

		}
	};
});
