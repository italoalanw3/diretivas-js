appRastrus.directive("mascaraImei", function($filter){
	return {
		require: "ngModel",
		link: function(scope, element, attrs, ctrl){
			var _fromatIMEI = function(imei){
				if (imei != null) {
					imei = imei.replace(/[^0-9]+/g, '');
					if (imei.length > 6)
						imei = imei.substring(0, 6) + '-' + imei.substring(6);
					if (imei.length > 9)
						imei = imei.substring(0, 9) + '-'+ imei.substring(9);
					if (imei.length > 16)
						imei = imei.substring(0, 16) + '-' + imei.substring(16, 17);
	            }
	            return imei;
			}
			element.bind("keyup", function(){
				ctrl.$setViewValue(_fromatIMEI(ctrl.$viewValue));
				ctrl.$render();				
			});

			ctrl.$parsers.push(function (value){
				value = value.replace(/[^0-9]+/g, '');				
				return value;
			});

			ctrl.$formatters.push(function(value){				
				return $filter("mascaraImei")(value);
			});
		}
	};
});