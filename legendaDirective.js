appRastrus.directive('legenda',function(){
	return {	    
	    templateUrl:'view/legenda.html',
	    restrict: 'AE',
	    replace: true,
	    scope:{
	    	editar: "@",
	    	excluir: "@",
	    	exportar: "@",
	    	upload: "@",
	    	encerrar: "@",
	    	visualizar: "@",
	    	verdetalhe: "@",
	    	agendar: "@",
	    	aprovar: "@",
	    	reprovar: "@",
	    	imprimir: "@",
	    	aviso: "@",
	    	cancelar: "@",
	    }
	}
});