appRastrus.directive("mascaraDigito", function($filter){
	return {
		require: "ngModel",
		link: function(scope, element, attrs, ctrl){
			var _frormatInteiro = function(inteiro){				
				if (inteiro != null)
					inteiro = inteiro.replace(/[^0-9xX]+/g, '');
	            return inteiro;
			}

			element.bind("keyup", function(){
				ctrl.$setViewValue(_frormatInteiro(ctrl.$viewValue));							
				ctrl.$render();				
			});	
		}
	};
});