appRastrus.directive("mascaraInteiro", function($filter){
	return {
		require: "ngModel",
		link: function(scope, element, attrs, ctrl){
			var _frormatInteiro = function(inteiro){				
				if (inteiro != null)
					inteiro = inteiro.replace(/[^0-9]+/g, '');
	            return inteiro;
			}

			element.bind("keyup", function(){
				ctrl.$setViewValue(_frormatInteiro(ctrl.$viewValue));							
				ctrl.$render();				
			});	
		}
	};
});